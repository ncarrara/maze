/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
import java.util.Arrays;
import model.Model;
import model.Model.Case;
import org.junit.After;
import org.junit.AfterClass;
import static org.junit.Assert.assertEquals;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;

/**
 *
 * @author Pioupiou
 */
public class ModelTest {

    public ModelTest() {
    }

    @BeforeClass
    public static void setUpClass() {
    }

    @AfterClass
    public static void tearDownClass() {
    }

    @Before
    public void setUp() {
    }

    @After
    public void tearDown() {
    }

//    @Test
    public void hello() {

        int taille = 2;

        Case a = new Case("a", 0, 1, 0, 0, 0);
        Case b = new Case("b", 0, 0, 1, 0, 0);
        Case c = new Case("c", 0, 0, 0, 1, 0);
        Case d = new Case("d", 1, 0, 0, 0, 0);

        // a-> c -> d -> b
        Case[] monde = {
            a, b,
            c, d
        };

        Model m = new Model(monde, taille);

//        System.out.println("m : " + m);

        assertEquals(m.getMc().getEtatCourant(),a);
        m.run();
        assertEquals(m.getMc().getEtatCourant(),c);
//        System.out.println(Arrays.toString(m.getMc().getDistribution()));
        m.run();
        assertEquals(m.getMc().getEtatCourant(),d);
//        System.out.println(Arrays.toString(m.getMc().getDistribution()));
        m.run();
        assertEquals(m.getMc().getEtatCourant(),b);
//        System.out.println(Arrays.toString(m.getMc().getDistribution()));
        m.run();
        assertEquals(m.getMc().getEtatCourant(),a);
//        System.out.println(Arrays.toString(m.getMc().getDistribution()));
        m.run();
        assertEquals(m.getMc().getEtatCourant(),c);
//        System.out.println(Arrays.toString(m.getMc().getDistribution()));
    }
    
    @Test
    public void hello2() {

        int taille = 2;
        //                     h    b    g     d  rester
        Case a = new Case("a", 0.0, 0.33, 0.2, 0.33, 0.33);
        Case b = new Case("b", 0.0, 0.33, 0.33, 0.0, 0.33);
        Case c = new Case("c", 0.33, 0.0, 0.0, 0.33, 0.33);
        Case d = new Case("d", 0.33, 0.0, 0.33, 0.0, 0.33);

        Case[] monde = {
            a, b,
            c, d
        };

        Model m = new Model(monde, taille);

//        System.out.println("m : " + m);
        
        System.out.println("=======================> "+m.getMc());
        System.out.println("----------------------------------------------------------");
        System.out.println(Arrays.toString(m.getMc().getDistribution()));
        System.out.println("----------------------------------------------------------");
        m.run();
        System.out.println(Arrays.toString(m.getMc().getDistribution()));
        System.out.println("----------------------------------------------------------");
        m.run();
        System.out.println(Arrays.toString(m.getMc().getDistribution()));
        System.out.println("----------------------------------------------------------");
        m.run();
        System.out.println(Arrays.toString(m.getMc().getDistribution()));
        System.out.println("----------------------------------------------------------");
        m.run();
        System.out.println(Arrays.toString(m.getMc().getDistribution()));
        System.out.println("----------------------------------------------------------");
        m.run();
        System.out.println(Arrays.toString(m.getMc().getDistribution()));
    }
}
