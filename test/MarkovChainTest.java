/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
import mc.MarkovChain;
import org.junit.After;
import org.junit.AfterClass;
import static org.junit.Assert.assertEquals;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;

/**
 *
 * @author Pioupiou
 */
public class MarkovChainTest {

    MarkovChain mc;

    public MarkovChainTest() {
    }

    @BeforeClass
    public static void setUpClass() {
    }

    @AfterClass
    public static void tearDownClass() {
    }

    private String[] etats;
    
    @Before
    public void setUp() {
        etats = new String[3];
        etats[0] = "A";
        etats[1] = "B";
        etats[2] = "C";
        
    }

    @After
    public void tearDown() {
    }

    // TODO add test methods here.
    // The methods must be annotated with annotation @Test. For example:
    //
     @Test
     public void deterministe() {
         double[][] transitions // A->B->C->A
                = {
                    {0, 0, 1},
                    {1, 0, 0},
                    {0, 1, 0}
                };
        double[] distribution = {1,0,0}; // on commence en A
        
        mc = new MarkovChain(etats, transitions, distribution);
        
        assertEquals(etats[mc.walk()],"B");
        assertEquals(etats[mc.walk()],"C");
        assertEquals(etats[mc.walk()],"A");
        assertEquals(etats[mc.walk()],"B");
     }
     
     @Test
     public void nondeterministe() {
         double[][] transitions // A->B->C->A
                = {
                    {0, 0, 0.9},
                    {0.9, 0, 0},
                    {0, 0.9, 0}
                };
        double[] distribution = {1,0,0}; // on commence en A
        
        mc = new MarkovChain(etats, transitions, distribution);
        
        assertEquals(etats[mc.walk()],"B");
        assertEquals(etats[mc.walk()],"C");
        assertEquals(etats[mc.walk()],"A");
        assertEquals(etats[mc.walk()],"B");
     }
}
