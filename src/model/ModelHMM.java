/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package model;

import hmm.HidenMarkovModel;
import static model.Model.BAS;
import static model.Model.DROITE;
import static model.Model.GAUCHE;
import static model.Model.HAUT;
import static model.Model.RESTER;
import tool.Myst;

/**
 *
 * @author CARRARA Nicolas carrara1u@etu.univ-lorraine.fr
 */
public class ModelHMM extends Model {

    public ModelHMM(Case[] monde, int taille) {
        super(monde, taille);
        flouterLesCapteurs();
    }

    private void flouterLesCapteurs() {
        double d = Math.random() / 100.0;
        double[][] probasObservationsEtats = ((HidenMarkovModel) mc).getProbasObservationEtat();
        System.out.println("avant flou");
        Myst.afficherMatrice(probasObservationsEtats);
        double somme = 0.0;
        for (int i = 0; i < probasObservationsEtats.length; i++) {
            somme = 0.0;
            double s = d * probasObservationsEtats[0].length;
            for (int j = 0; j < probasObservationsEtats[0].length; j++) {
                s += probasObservationsEtats[i][j];
            }
            for (int j = 0; j < probasObservationsEtats[0].length; j++) {
                probasObservationsEtats[i][j] = +(probasObservationsEtats[i][j] + d) / s;
                somme += probasObservationsEtats[i][j];
            }
            System.out.println("somme : " + somme);
        }
        System.out.println("apres flou ");
        Myst.afficherMatrice(probasObservationsEtats);
    }

    @Override
    protected void constructMarkovChain() {
        int[] etats = new int[getMonde().length];
        double[][] transitions = new double[etats.length][etats.length];

        for (int k = 0; k < getMonde().length; k++) {
            int x = k / getTaille();
            int y = k % getTaille();
            Case c = getMonde()[k];
            double[] probas = c.getProbasDirections();

            int xh, xb, yg, yd;
            if ((xh = (x - 1)) >= 0) {
                transitions[xh * getTaille() + y][k] = probas[HAUT];
            }
            if ((xb = (x + 1)) < getTaille()) {
                transitions[xb * getTaille() + y][k] = probas[BAS];
            }
            if ((yg = (y - 1)) >= 0) {
                transitions[x * getTaille() + yg][k] = probas[GAUCHE];
            }
            if ((yd = (y + 1)) < getTaille()) {
                transitions[x * getTaille() + yd][k] = probas[DROITE];
            }
            transitions[k][k] = probas[RESTER];
        }
        double[] ditribution_initiale = new double[getMonde().length];
        ditribution_initiale[getPosition()] = 1;

        double[][] probasObservationsEtats = new double[taille / 2 + taille % 2][taille * taille];
        Myst.afficherMatrice(probasObservationsEtats);

        for (int k = 0; k < taille - 1; k++) {
//            System.out.println("k : " + k);
            int tailleCouche = taille - 2 * k;
//            System.out.println("tailleCouche : "+tailleCouche);
            int nbEtats = tailleCouche == 1 ? 1 : 4 * tailleCouche - 4;
//            System.out.println("nbEtat : " + nbEtats);
            double probas = 1. / nbEtats;

            int x1 = k;
            for (int y1 = k; y1 < taille - k; y1++) {
//                System.out.println("a" + x1 + " " + y1);
                probasObservationsEtats[k][x1 * taille + y1] = probas;
            }

            x1 = taille - k - 1;
            for (int y1 = k; y1 < taille - k; y1++) {
//                System.out.println("b" + x1 + " " + y1);
                probasObservationsEtats[k][x1 * taille + y1] = probas;
            }

            int y2 = k;
            for (int x2 = k + 1; x2 < taille - k; x2++) {
//                System.out.println("c" + x2 + " " + y2);
                probasObservationsEtats[k][x2 * taille + y2] = probas;
            }
            y2 = taille - k - 1;
            for (int x2 = k; x2 < taille - k - 1; x2++) {
//                System.out.println("d" + x2 + " " + y2);
                probasObservationsEtats[k][x2 * taille + y2] = probas;
            }
        }
        System.out.println("probasObservationsEtats");
        Myst.afficherMatrice(probasObservationsEtats);
        mc = new HidenMarkovModel(getMonde(), transitions, probasObservationsEtats, ditribution_initiale);
    }

    @Override
    public void run() {
        position = getMc().walk();
        int x = position / taille;
        int y = position % taille;
        int indexCoucheObservation = getCouche(x, y);
//        System.out.println("indiceCoucheObservation : "+indexCoucheObservation);
        ((HidenMarkovModel) getMc()).computeNextBelief(indexCoucheObservation);
//        System.out.println("beliefs : "+Arrays.toString(((HidenMarkovModel) getMc()).getCurrentBeliefs()));
        update();
    }

    @Override
    public void reset() {
        super.reset();
        flouterLesCapteurs();
    }

    /**
     * super méga sale
     *
     * @param x
     * @param y
     * @return
     */
    private int getCouche(int x, int y) {
        boolean found = false;
        int k = 0;
        for (k = 0; !found && k < taille - 1; k++) {
            int x1 = k;
            for (int y1 = k; !found && y1 < taille - k; y1++) {
                found = x == x1 && y == y1;
            }
            x1 = taille - k - 1;
            for (int y1 = k; !found && y1 < taille - k; y1++) {
                found = x == x1 && y == y1;
            }
            int y2 = k;
            for (int x2 = k + 1; !found && x2 < taille - k; x2++) {
                found = x == x2 && y == y2;
            }
            y2 = taille - k - 1;
            for (int x2 = k; !found && x2 < taille - k - 1; x2++) {
                found = x == x2 && y == y2;
            }
        }
        return k - 1;
    }

}
