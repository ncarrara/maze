/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package model;

import java.util.Arrays;
import java.util.Observable;
import mc.MarkovChain;

/**
 *
 * @author CARRARA Nicolas carrara1u@etu.univ-lorraine.fr
 */
public class Model extends Observable {

    protected MarkovChain mc;

    public static final int HAUT = 0;
    public static final int BAS = 1;
    public static final int GAUCHE = 2;
    public static final int DROITE = 3;
    public static final int RESTER = 4;

    public void update() {
        setChanged();
        notifyObservers();
    }

   
    public static class Case {

        private double[] probasDirections;

        private String name;

        public Case(String name, double proba_haut, double proba_bas, double proba_gauche, double proba_droite, double proba_rester) {
            probasDirections = new double[5];
            probasDirections[HAUT] = proba_haut;
            probasDirections[DROITE] = proba_droite;
            probasDirections[GAUCHE] = proba_gauche;
            probasDirections[RESTER] = proba_rester;
            probasDirections[BAS] = proba_bas;
            this.name = name;
        }

        @Override
        public String toString() {
            return getName();//+" : "+Arrays.toString(probasDirections);
        }

        /**
         * @return the probasDirections
         */
        public double[] getProbasDirections() {
            return probasDirections;
        }

        /**
         * @return the name
         */
        public String getName() {
            return name;
        }

    }

    private Case[] monde;

    protected int position = 0;

    protected int taille;

    public Model(Case[] monde, int taille) {
        this.taille = taille;
        this.monde = monde;
        constructMarkovChain();
    }

    protected void constructMarkovChain() {
        int[] etats = new int[getMonde().length];
        double[][] transitions = new double[etats.length][etats.length];

        for (int k = 0; k < getMonde().length; k++) {
            int x = k / getTaille();
            int y = k % getTaille();
            Case c = getMonde()[k];
            double[] probas = c.getProbasDirections();

            int xh, xb, yg, yd;
            if ((xh = (x - 1)) >= 0) {
                transitions[xh * getTaille() + y][k] = probas[HAUT];
            }
            if ((xb = (x + 1)) < getTaille()) {
                transitions[xb * getTaille() + y][k] = probas[BAS];
            }
            if ((yg = (y - 1)) >= 0) {
                transitions[x * getTaille() + yg][k] = probas[GAUCHE];
            }
            if ((yd = (y + 1)) < getTaille()) {
                transitions[x * getTaille() + yd][k] = probas[DROITE];
            }
            transitions[k][k] = probas[RESTER];
        }
        double[] ditribution_initiale = new double[getMonde().length];
        ditribution_initiale[getPosition()] = 1;
        mc = new MarkovChain(getMonde(), transitions, ditribution_initiale);
    }

    public void run() {
        position = getMc().walk();
        update();
    }

    @Override
    public String toString() {
        return Arrays.toString(getMonde());
    }

    public void reset() {
        position = 0;
        getMc().reset();
        update();
    }

     /**
     * @return the mc
         */
    public MarkovChain getMc() {
        return mc;
    }

    /**
     * @return the monde
     */
    public Case[] getMonde() {
        return monde;
    }

    /**
     * @return the position
     */
    public int getPosition() {
        return position;
    }

    /**
     * @return the taille
     */
    public int getTaille() {
        return taille;
    }

    
}
