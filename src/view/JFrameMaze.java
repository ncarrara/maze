/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package view;

import java.awt.BorderLayout;
import java.awt.GridLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.IOException;
import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.Timer;
import model.Model;
import model.ModelHMM;

/**
 *
 * @author CARRARA Nicolas <nicolas.carrara1@etu.univ-lorraine.fr>
 */
public class JFrameMaze extends JFrame {

    public static void main(String[] args) throws IOException {

        int taille = 3;
        Model.Case a = new Model.Case("a", 0.0, 1. / 3., 0.0, 0.0, 2. / 3.);
        Model.Case b = new Model.Case("b", 0.0, 1. / 12., 1. / 12., 1. / 12., 3. / 4.);
        Model.Case c = new Model.Case("c", 0.0, 0.0, 1. / 50., 0.0, 49. / 50.);
        Model.Case d = new Model.Case("d", 1. / 2., 0.0, 0.0, 1. / 4., 1. / 4.);
        Model.Case e = new Model.Case("e", 1. / 4., 1. / 4., 1. / 4., 0.0, 1. / 4.);
        Model.Case f = new Model.Case("f", 0.0, 0.0, 0.0, 0.0, 0.0);
        Model.Case g = new Model.Case("g", 1. / 2., 0.0, 0.0, 1. / 4., 1. / 4.);
        Model.Case h = new Model.Case("h", 1. / 4., 0.0, 1. / 4., 1. / 4., 1. / 4.);
        Model.Case i = new Model.Case("i", 0.0, 0.0, 3. / 4., 0.0, 1. / 4.);

        Model.Case[] monde = {
            a, b, c,
            d, e, f,
            g, h, i
        };

//        int taille = 2 ;
//        Model.Case a = new Model.Case("a", 0, 1, 0, 0, 0);
//        Model.Case b = new Model.Case("b", 0, 0, 1, 0, 0);
//        Model.Case c = new Model.Case("c", 0, 0, 0, 1, 0);
//        Model.Case d = new Model.Case("d", 1, 0, 0, 0, 0);
////        // a-> c -> d -> b
////
//        Model.Case[] monde = {
//            a, b,
//            c, d
//        };
//
//        Model m = new Model(monde, taille); decommenter cette ligne et commenter la suivantepour avoir seulement la chaine de markov
        Model m = new ModelHMM(monde, taille);
        System.out.println("" + m.getMc().toString());
        System.out.println("m : " + m);
        new JFrameMaze(m);
    }

    final JButton n = new JButton("next");
    final JButton g = new JButton("go");
    final JButton s = new JButton("stop");
    final JButton r = new JButton("reset");
    final Timer timer;
    final Model model;

    public JFrameMaze(final Model model/*,int infvie,int supvie,int infnaissance,int supnaissance*/) throws IOException {
        super(/*"Jeu de la vie vie : "+"["+infvie+","+supvie+"]"+" naissance : "+"["+infnaissance+","+supnaissance+"]"*/);
        JPanelMaze xyz = new JPanelMaze(model, 640, 640);
        add(xyz);
        JPanel south = new JPanel(new GridLayout(1, 4));
        this.model = model;
        timer = new Timer(50, new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                next();
            }
        });

        n.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                next();
            }
        });

        r.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                reset();
            }
        });

        g.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                go();
            }
        });

        s.setEnabled(false);
        s.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                stop();
            }
        });

        south.add(r);
        south.add(n);
        south.add(g);
        south.add(s);
        add(south, BorderLayout.SOUTH);
        setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        pack();
        setVisible(true);
    }

    public void stop() {
        n.setEnabled(true);
        g.setEnabled(true);
        timer.stop();
    }

    public void next() {
//        if (model.getRuns() < model.getIterationsMax()) {
        model.run();
//        } else {
//            stop();
//            maxiteration();
//        }
    }

    public void reset() {
        n.setEnabled(true);
        s.setEnabled(false);
        g.setEnabled(true);
        timer.stop();
        model.reset();
    }

    public void go() {
        g.setEnabled(false);
        n.setEnabled(false);
        s.setEnabled(true);
        timer.start();
    }

//    public void maxiteration() {
//        g.setEnabled(false);
//        n.setEnabled(false);
//        s.setEnabled(false);
//    }
}
