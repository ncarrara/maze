/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package view;

import hmm.HidenMarkovModel;
import java.awt.Color;
import java.awt.Dimension;
import java.awt.Graphics;
import java.awt.Graphics2D;
import java.awt.Rectangle;
import java.awt.RenderingHints;
import java.awt.geom.Line2D;
import java.awt.geom.Rectangle2D;
import java.util.Observable;
import java.util.Observer;
import javax.swing.JPanel;
import model.Model;
import model.Model.Case;
import model.ModelHMM;

/**
 *
 * @author CARRARA Nicolas <nicolas.carrara1@etu.univ-lorraine.fr>
 */
public class JPanelMaze extends JPanel implements Observer {

    int width, height;

    int size;

    int cols;

    private Model model;

    private int xdt;
    private int ydt;

    JPanelMaze(Model model, int w, int h) {
        model.addObserver(this);
        //setSize(width = w, height = h);
        setPreferredSize(new Dimension(height = h, width = w));
        size = model.getTaille();
        this.model = model;
        xdt = height / size;
        ydt = width / size;

    }

    @Override
    protected void paintComponent(Graphics g) {
        super.paintComponent(g);
        Graphics2D g2 = (Graphics2D) g;

        width = getSize().width;
        height = getSize().height;
        xdt = height / size;
        ydt = width / size;
        g2.setRenderingHint(RenderingHints.KEY_ANTIALIASING,
                RenderingHints.VALUE_ANTIALIAS_ON);
        int position = model.getPosition();
        int k = position / model.getTaille();
        int l = position % model.getTaille();
        int cx = l * xdt + xdt / 2 - xdt / 6;
        int cy = k * ydt + ydt / 2 - xdt / 6;
        g2.setColor(Color.BLACK);
        g2.fillOval(cx, cy, xdt / 3, ydt / 3);
        for (int i = 0; i < size; i++) {
            for (int j = 0; j < size; j++) {
                int x = i * xdt;
                int y = j * ydt;
                Rectangle2D rect = new Rectangle(x, y, xdt - 1, ydt - 1);
                Case c = model.getMonde()[j * model.getTaille() + i];
                g2.setColor(Color.RED);
                g2.draw(rect);

                int xmid = x + xdt / 2;
                int ymid = y + ydt / 2;
                g2.setColor(Color.BLACK);
                g2.drawString("\n prior (" + String.format("%.2f", model.getMc().getDistribution()[j * model.getTaille() + i]) + ")", x + xdt / 10, y + ydt / 10);
                g2.drawString(c.getName(), x + xdt / 10, y + ydt / 2);
                g2.setColor(Color.BLACK);
                if (model instanceof ModelHMM) {
//                    System.out.println(""+((HidenMarkovModel) model.getMc()).getCurrentBeliefs());
                    g2.drawString("belief" + "(" + String.format("%.2f", ((HidenMarkovModel) model.getMc()).getCurrentBeliefs()[j * model.getTaille() + i]) + ")", x + xdt / 10, y + 9 * ydt / 10);
                }
                g2.setColor(Color.BLUE);
                if (c.getProbasDirections()[Model.DROITE] != 0.0) {
                    Line2D droite = new Line2D.Double(xmid, ymid, x + xdt, y + ydt / 2);

                    g2.draw(droite);
                    g2.drawString(String.format("%.2f", c.getProbasDirections()[Model.DROITE]) + "", xmid + xdt / 4, ymid);
                }
                if (c.getProbasDirections()[Model.GAUCHE] != 0.0) {
                    Line2D droite = new Line2D.Double(xmid, ymid, x, y + ydt / 2);
                    g2.draw(droite);
                    g2.drawString(String.format("%.2f", c.getProbasDirections()[Model.GAUCHE]) + "", xmid - xdt / 4, ymid);
                }
                if (c.getProbasDirections()[Model.HAUT] != 0.0) {
                    Line2D droite = new Line2D.Double(xmid, ymid, xmid, ymid - ydt / 2);
                    g2.draw(droite);
                    g2.drawString(String.format("%.2f", c.getProbasDirections()[Model.HAUT]) + "", xmid, ymid - ydt / 4);
                }
                if (c.getProbasDirections()[Model.BAS] != 0.0) {
                    Line2D droite = new Line2D.Double(xmid, ymid, xmid, ymid + ydt / 2);
                    g2.draw(droite);
                    g2.drawString(String.format("%.2f", c.getProbasDirections()[Model.BAS]) + "", xmid, ymid + ydt / 4);
                }
                if (c.getProbasDirections()[Model.RESTER] != 0.0) {
                    g2.drawString(String.format("%.2f", c.getProbasDirections()[Model.RESTER]) + "", xmid, ymid);
                }
            }
        }

    }

    @Override
    public void update(Observable o, Object arg) {
        paintComponent(getGraphics());
    }

}
